**INSTALLATION**
==========

.. toctree::
   :maxdepth: 2

   basic-installation/index
   release-installation/index
   release-notes/index
   