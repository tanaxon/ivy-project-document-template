.. _ajax-validator:

Deactivate Ajax validator
-------------

1. Open the file **web.xml** in the folder **engine\\webapps\\ivy\\WEB-INF** and add the following code:

.. literalinclude:: includes/ajax-validator.xml
  :language: xml
  :linenos:
  
2. Restart the server for the changes to take effect.