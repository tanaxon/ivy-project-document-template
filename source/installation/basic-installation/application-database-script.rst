.. _application-database-script:

Create new application database script
-------------

.. literalinclude:: includes/application-database-script.sql
  :language: sql
  :linenos:
  