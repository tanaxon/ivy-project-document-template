.. basic-installation:

Basic installation
************
-  **Important Note:**

   This application will run on server Axon.ivy Engine {engineVersion}
   Overview of modules and connected external systems:

   -  Application Modules Chart:
      |{applicationChartImage}|
   -  An editable `UMLet<https://www.umlet.com>`_file also available: DeploymentMap.uxf

-  **Modules Axon.ivy Engine**

   -   :ref:`{projectName} Application Modules and Configuration <application-modules-configuration>` 
   -   :ref:`Application Database <application-database>` 
   -   :ref:`Create Application Database Script <application-database-script>` 
   -   :ref:`Setting default language, notification email and endpage <setting>` 

-  **Ajax Validator**

   -   :ref:`Deactivate Ajax validator <ajax-validator>` 


-  **Configure basic permission**

   -   :ref:`Permission configuration <permission-configuration>` 


-  **Web Service Configuration**

   -   :ref:`Set up web service configuration <>` 
   -   :ref:`Set up REST service configuration <>` 
   -   :ref:`Set up web service logging <>` 
   -   :ref:`Add Proxy to Webservice <>` 


-  **Set up Portal**

   -  This project uses the Axon.ivy Portal Standard, see general Portal documentation.


-  **Tracking for case and task custom_varchar**

   -   :ref:`Tracking for case and task custom_varchar <case-task-custom-varchar>` 


-  **Roles and permissions for WebServices**

   -   :ref:`Roles and permissions for WebServices <roles-permissions>` 
