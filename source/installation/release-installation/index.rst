.. _release-installation:

Installation
************
   The installation section describes all the steps, that are necessary to install and setup the Process Application. If you install the application for the first time than it's important to start with the Basic installation. It describes all the initial steps, that must be done for the first installation.

-   :ref:`Basic Installation <basic-installation/index>` 
	
Release Installations
^^^^^^^^^^^^^^^^^^^^^
   If the application is already installed and initial prepared, than refer to the Release Installation Steps, that are provided, here you will only find those steps, that are necessary to install this release.

-   :ref:`Release for version {releaseVersionNumber} <version_001>` 
