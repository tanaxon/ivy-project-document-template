.. _custom-error-pages:

Custom Error Pages
************


1. Open the file **web.xml** in the folder **engine\\webapps\\ivy\\WEB-INF** and add the following code:

.. literalinclude:: includes/custom-error-page.xml
  :language: xml
  :linenos:
  
2. Copy **custom-error-page.xhtml** to **engine\\webapps\\ivy**.
3. Copy **custom-error-page folder** to **engine\\webapps\ivy\\resources**.
4. Restart the server for the changes to take effect.

**IMPORTANT Note:** To handle all error page cases, it is needed to have Ivy Application specific error pages with specific URLs to the Portal-Home of each project.
This must be configured inside the custom-error-page.xhtml (see **<c:choose>...</c:choose>** block) for each **project and environment (DEV, QA and PROD)** separately

Download all resources ``custom_error_page_resource.zip``