.. _portal-document:

{projectName} Document
***************

.. toctree::
   :maxdepth: 2
   :caption: {projectName} Document
   
   project/index
   installation/index
   quicklinks/index
   
